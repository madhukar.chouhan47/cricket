import { ApolloClient, InMemoryCache, ApolloLink } from '@apollo/client';
import { onError } from 'apollo-link-error';

import { createHttpLink } from 'apollo-link-http';
export const RESTAPI = process.env.REACT_APP_REST_ENDPOINT;
let graphqlUrl = process.env.REACT_APP_GRAPHQL_ENDPOINT;

const httpLink = createHttpLink({
  uri: graphqlUrl
});
const cache = new InMemoryCache();

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: localStorage.getItem('user.token') || null
    }
  });
  return forward(operation);
});

const errorLink = onError(({ graphQLErrors }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message }) => {
      if (message.includes('Auth token is expired')) {
        logoutUser();
      }
    });
});

const link = ApolloLink.from([errorLink, authMiddleware, httpLink]);

export const client = new ApolloClient({
  cache,
  link: link
});

const logoutUser = () => {
  localStorage.clear();
  client.resetStore();
  window.location.href = '/login';
  window.location.reload(true);
};
