import React from 'react';
import { render } from 'react-dom';
import MainApp from './MainApp';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/style.scss';

const WebApp = () => (
  <BrowserRouter>
    <MainApp />
  </BrowserRouter>
);

render(<WebApp />, document.querySelector('#root'));
