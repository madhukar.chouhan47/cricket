import React from 'react';
import { ApolloProvider } from '@apollo/client';

import App from './routes';
import { client } from './config';

const MainApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

export default MainApp;
