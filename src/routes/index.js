import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Listing from '../pages/ListingPage';
import TeamForm from '../pages/TeamForm';
import TeamList from '../pages/TeamList';

const App = () => {
  return (
    <div className="cricket-bg">
      <Switch>
        <Route exact path="/teamlist" component={TeamList}></Route>
        <Route exact path="/listing" component={Listing}></Route>
        <Route exact path="/" component={TeamForm}></Route>
      </Switch>
    </div>
  );
};
export default App;
