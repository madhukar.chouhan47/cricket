import React from 'react';
import { Row, Col } from 'reactstrap';

import NumberCard from './components/NumberCard';
import Team from './components/Team';
import UserCard from './components/UserCard';

const TeamList = () => {
  const teamMembers = [
    {
      teamName: 'bullians',
      name: 'Ankit Negi',
      strength: 'batsman',
      level: 'a',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Vineet Agarwal',
      strength: 'bowler',
      level: 'a',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Deepak Tomar',
      strength: 'allrounder',
      level: 'a',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Lokendra Kushwah',
      strength: 'batsman',
      level: 'b',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Sumit Patrikar',
      strength: 'batsman',
      level: 'b',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Bhuvan Parasar',
      strength: 'bowler',
      level: 'b',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Rohit Shrivastava',
      strength: 'batsman',
      level: 'b',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Neelesh Wabhale',
      strength: 'allrounder',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Antim Chouhan',
      strength: 'batsman',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Chetan Gehlot',
      strength: 'batsman',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Virendra Patidar',
      strength: 'bowler',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Nikhil Laky',
      strength: 'batsman',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Aman Soni',
      strength: 'allrounder',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Neeraj Negi',
      strength: 'batsman',
      level: 'c',
      basePrice: 100000
    },
    {
      teamName: 'bullians',
      name: 'Vishal Solanki',
      strength: 'allrounder',
      level: 'c',
      basePrice: 100000
    }
  ];
  return (
    <div className="hundred-vh">
      <Row className="pt-5">
        <Col xl="5">
          <UserCard />
          <Row className="pt-2">
            <Col className="p-0">
              <NumberCard
                teamName="Bullians"
                color="primary"
                remainingAmount="0"
              />
            </Col>
            <Col className="p-0">
              <NumberCard
                teamName="Unicorns"
                color="secondary"
                remainingAmount="250000"
              />
            </Col>
            <Col className="p-0">
              <NumberCard
                teamName="Mighty Lions"
                color="warning"
                remainingAmount="500000"
              />
            </Col>
            <Col className="p-0">
              <NumberCard
                teamName="Sharks"
                color="info"
                remainingAmount="850000"
              />
            </Col>
          </Row>
        </Col>
        <Col className="pl-0">
          <Row>
            <Col className="p-0">
              <Team
                teamName="Bullians"
                color="primary"
                teamMembersList={teamMembers}
              />
            </Col>
            <Col className="pr-0">
              <Team
                teamName="Unicorns"
                color="secondary"
                teamMembersList={teamMembers}
              />
            </Col>
            <Col className="pr-0">
              <Team
                teamName="Mighty Lions"
                color="warning"
                teamMembersList={teamMembers}
              />
            </Col>
            <Col className="pr-0">
              <Team
                teamName="Sharks"
                color="info"
                teamMembersList={teamMembers}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default TeamList;
