import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup, ListGroupItem } from 'reactstrap';

import batsman from '../../../../assets/icons/batsman.svg';
import bowler from '../../../../assets/icons/bowler.svg';
import allRounder from '../../../../assets/icons/allrounder.svg';

const Team = (props) => {
  const strengthImage = (member) => {
    return (
      <img
        src={
          member.strength === 'batsman'
            ? batsman
            : member.strength === 'bowler'
            ? bowler
            : member.strength === 'allrounder'
            ? allRounder
            : allRounder
        }
        className="batsman-icon"
      />
    );
  };

  const listColor = (member) => {
    return member.level === 'a'
      ? 'warning'
      : member.level === 'b'
      ? 'success'
      : member.level === 'c'
      ? 'danger'
      : '';
  };

  return (
    <div>
      <ListGroup>
        <ListGroupItem className={`text-center text-light bg-${props.color}`}>
          <b className="text-uppercase">{props.teamName}</b>
        </ListGroupItem>
        {props.teamMembersList.map((member, i) => (
          <ListGroupItem
            key={i}
            className="font-weight-bold"
            color={listColor(member)}
            tag="a"
            href="#"
            action
          >
            {strengthImage(member)}
            {member.name}
          </ListGroupItem>
        ))}
      </ListGroup>
    </div>
  );
};

Team.propTypes = {
  color: PropTypes.any,
  teamName: PropTypes.any,
  teamMembersList: PropTypes.any
};

export default Team;
