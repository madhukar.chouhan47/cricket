import React from 'react';
import { Row, Col, Card, CardImg, Button } from 'reactstrap';

const UserCard = () => {
  return (
    <div>
      <Card>
        <Row className="border border-5">
          <Col xl={4} className="p-0 border-right">
            <CardImg
              top
              src="https://thumbs.dreamstime.com/b/avatar-man-soccer-player-graphic-sports-clothes-front-view-over-isolated-background-illustration-73244786.jpg"
              alt="Card image cap"
              className="avatar-image"
            />
          </Col>
          <Col>
            <div className="d-flex justify-content-end pt-2">
              <span>
                <b>BASE PRICE</b>
              </span>
            </div>
            <div className="pb-3">
              <b className="text-primary">
                <span className="badge bg-warning text-dark px-3 py-2">
                  <b>ALL ROUNDER</b>
                </span>
              </b>
            </div>
            <Row>
              <Col className="p-0">
                <div className="card-text-m">MATCHES</div>
                <div className="card-text-m">RUNS</div>
                <div className="card-text-m">6s - | 4s -</div>
              </Col>
              <Col className="p-0">
                <div className="card-text-m">MATCHES</div>
                <div className="card-text-m">OVERS</div>
                <div className="card-text-m">WICKETS</div>
              </Col>
            </Row>
            <div className="py-3">
              <Button className="btn btn-secondary mr-2">Next Player</Button>
              <Button className="btn btn-secondary">Sold</Button>
            </div>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default UserCard;
