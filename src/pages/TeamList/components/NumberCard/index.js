import React from 'react';
import PropTypes from 'prop-types';

const NumberCard = (props) => {
  return (
    <div>
      <div className="border border-5 m-1 p-1 bg-white">
        <div className={`text-center text-light bg-${props.color}`}>
          <b className="text-uppercase">{props.teamName}</b>
        </div>
        <div className="text-center py-1">
          <b>(14)</b>
        </div>
        <div className="text-center py-1">
          <b>0</b>
        </div>
        <div className="d-flex justify-content-center">
          <div>
            <div className="btn-group-vertical">
              <button
                type="button"
                className={`btn btn-outline-${props.color}`}
              >
                0 K
              </button>
              <button
                type="button"
                className={`btn btn-outline-${props.color}`}
              >
                1 L
              </button>
            </div>
          </div>
          <div>
            <div className="btn-group-vertical">
              <button
                type="button"
                className={`btn btn-outline-${props.color}`}
              >
                50K
              </button>
              <button
                type="button"
                className={`btn btn-outline-${props.color}`}
              >
                2 L
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className={`border border-5 m-1 p-1 text-center text-light bg-${props.color}`}
      >
        <div>
          <div className="remaining-amount-text">Remaining Amount</div>
          <hr className="mb-0" />
          <div className="remaining-amount">{props.remainingAmount}</div>
        </div>
      </div>
    </div>
  );
};

NumberCard.propTypes = {
  teamName: PropTypes.any,
  color: PropTypes.any,
  remainingAmount: PropTypes.any
};

export default NumberCard;
