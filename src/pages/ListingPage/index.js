import React from 'react';

import TableView from './components/TableView';

const ListingPage = () => {
  return (
    <div className="d-flex justify-content-center">
      <div className="customer-container container-fluid">
        <div>
          <div>
            <TableView />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListingPage;
