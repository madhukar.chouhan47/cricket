import React from 'react';
import { useHistory } from 'react-router-dom';
import { Table } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const TableView = () => {
  const history = useHistory();
  const onRowClick = () => {
    history.push('/customerdetails');
  };
  return (
    <div>
      <Table borderless className="pcgcs-table" hover>
        <thead>
          <tr className="pcgcs-table-heading">
            <th className="pl-3">
              Cust ID <FontAwesomeIcon icon={faBars} />
            </th>
            <th>
              Customer Name <FontAwesomeIcon icon={faBars} />
            </th>
            <th>
              Customer Type <FontAwesomeIcon icon={faBars} />
            </th>
            <th>
              Account Type <FontAwesomeIcon icon={faBars} />
            </th>
            <th>
              Account Status <FontAwesomeIcon icon={faBars} />
            </th>
          </tr>
        </thead>
        <tbody>
          <tr onClick={onRowClick}>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">Mr. Michael Ozo</td>
            <td className="pl-3 text-success">Individual</td>
            <td className="pl-3 text-success">Saving</td>
            <td className="pl-3">Active</td>
          </tr>
          <tr>
            <td className="pl-3">3499829</td>
            <td className="pl-3">David S. K</td>
            <td className="pl-3 text-info">Corporate</td>
            <td className="pl-3 text-info">Current</td>
            <td className="pl-3">Active</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableView;
