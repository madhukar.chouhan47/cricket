module.exports = {
  extends: ['@commitlint/config-conventional'],
  parserPreset: 'conventional-changelog-angular',
  rules: {
    'type-enum': [
      2,
      'always',
      ['feat', 'fix', 'docs', 'refactor', 'test', 'chore']
    ],
    'scope-enum': [2, 'always', ['dsoqa-cli', 'dsoqa-tests']]
  }
};
